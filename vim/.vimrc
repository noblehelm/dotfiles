syntax on
filetype indent plugin on
colorscheme molokai
set number

map :NERDTree<c> <F2>
set pastetoggle=<F3>

" status bar for lightline
set laststatus=2
set noshowmode

" vim minimalist plugin manager
call plug#begin()
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'itchyny/lightline.vim'
Plug 'tpope/vim-sleuth'
Plug 'sheerun/vim-polyglot'
Plug 'w0rp/ale'
call plug#end()
