filetype plugin on
set nocompatible
set encoding=utf-8

set number
colorscheme gruvbox
set termguicolors
map <F2> :NERDTreeToggle<CR>
set pastetoggle=<F3>
set noshowmode
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>
imap <up> <nop>
imap <down> <nop>
imap <left> <nop>
imap <right> <nop>

au VimEnter * RainbowParentheses

" Language client stuff
" set hidden
" let g:LanguageClient_serverCommands = {
" 	\ 'rust': ['rustup', 'run', 'stable', 'rls'],
" 	\ }
" let g:LanguageClient_autoStart = 1
" nnoremap <F5> :call LanguageClient_contextMenu()<CR>
" noremap! jk <Esc>

" use deoplete
" let g:deoplete#enable_at_startup = 1
" let g:deoplete#sources#rust#racer_binary='/home/jr/.cargo/bin/racer'
" let g:deoplete#sources#rust#rust_source_path='$RUST_SRC_PATH'

" Ale options
let g:airline#extensions#ale#enabled = 1
let g:ale_linters = {'rust': ['rls']}
let g:ale_rust_rls_toolchain = 'stable'
"let g:ale_completion_enabled = 1

" Airline theme
let g:airline_theme='tomorrow'
let g:airline_powerline_fonts = 1
